﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vault
{
    class Program
    {
        static void Main(string[] args)
        {
            Fact.Reflection.Assembly Assembly = Fact.Reflection.Assembly.LoadFromFile(args[0]);
            List<string> pargs = new List<string>();
            for(int n = 1; n < args.Length; n++)
            {
                pargs.Add(args[n]);
            }
            Assembly.Run(pargs.ToArray());
        }
    }
}
